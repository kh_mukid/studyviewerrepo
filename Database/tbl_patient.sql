-- Table: public.patient

-- DROP TABLE public.patient;

CREATE TABLE public.patient
(
    id bigint NOT NULL,
    person_code character varying(30) COLLATE pg_catalog."default" NOT NULL,
    first_name character varying(50) COLLATE pg_catalog."default" NOT NULL,
    last_name character varying(50) COLLATE pg_catalog."default" NOT NULL,
    dob date NOT NULL,
    CONSTRAINT pk_patient_id PRIMARY KEY (id),
    CONSTRAINT uq_patient_person_code UNIQUE (person_code)
)

TABLESPACE pg_default;

ALTER TABLE public.patient
    OWNER to postgres;