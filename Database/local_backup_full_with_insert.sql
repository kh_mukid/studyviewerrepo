--
-- PostgreSQL database dump
--

-- Dumped from database version 13.1
-- Dumped by pg_dump version 13.1
-- Un-comment below line 09-27, if want restore with DB.

--SET statement_timeout = 0;
--SET lock_timeout = 0;
--SET idle_in_transaction_session_timeout = 0;
--SET client_encoding = 'UTF8';
--SET standard_conforming_strings = on;
--SELECT pg_catalog.set_config('search_path', '', false);
--SET check_function_bodies = false;
--SET xmloption = content;
--SET client_min_messages = warning;
--SET row_security = off;

--DROP DATABASE study_viewer;

--CREATE DATABASE study_viewer WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'English_United States.1252';


--ALTER DATABASE study_viewer OWNER TO postgres;

--\connect study_viewer

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 200 (class 1259 OID 41858)
-- Name: patient; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.patient (
    id bigint NOT NULL,
    person_code character varying(30) NOT NULL,
    first_name character varying(50) NOT NULL,
    last_name character varying(50) NOT NULL,
    dob date NOT NULL
);


ALTER TABLE public.patient OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 41875)
-- Name: patient_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.patient_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.patient_seq OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 41865)
-- Name: study; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.study (
    id bigint NOT NULL,
    patient_id bigint NOT NULL,
    name character varying(50) NOT NULL,
    description character varying(200),
    created_date timestamp without time zone NOT NULL,
    modified_date timestamp without time zone NOT NULL
);


ALTER TABLE public.study OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 41877)
-- Name: study_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.study_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.study_seq OWNER TO postgres;

--
-- TOC entry 2993 (class 0 OID 41858)
-- Dependencies: 200
-- Data for Name: patient; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.patient (id, person_code, first_name, last_name, dob) VALUES (1, 'AB123', 'KH Al', 'Mukid', '1987-08-12');
INSERT INTO public.patient (id, person_code, first_name, last_name, dob) VALUES (2, 'AB124', 'Abu', 'Manaf', '1987-08-12');
INSERT INTO public.patient (id, person_code, first_name, last_name, dob) VALUES (4, 'AP125', 'Test', 'Bull', '2021-05-01');
INSERT INTO public.patient (id, person_code, first_name, last_name, dob) VALUES (5, 'AP16556', 'fhfhj', 'fgngfjg', '2021-05-01');
INSERT INTO public.patient (id, person_code, first_name, last_name, dob) VALUES (6, 'AP222', 'Hector', 'Raphael', '2021-04-25');
INSERT INTO public.patient (id, person_code, first_name, last_name, dob) VALUES (7, 'AP126', 'Brookelly', 'Bull', '2021-05-28');
INSERT INTO public.patient (id, person_code, first_name, last_name, dob) VALUES (9, 'AP128', 'Test', 'fgngfjg', '2020-12-01');
INSERT INTO public.patient (id, person_code, first_name, last_name, dob) VALUES (10, 'AP122', 'fdgbdf', 'sfgbfd', '2021-05-28');
INSERT INTO public.patient (id, person_code, first_name, last_name, dob) VALUES (11, 'AP129', 'fgbfg', 'fgfsgs', '2021-05-28');
INSERT INTO public.patient (id, person_code, first_name, last_name, dob) VALUES (12, 'AP124', 'dfgbsd', 'dfsgsdg', '2021-05-28');
INSERT INTO public.patient (id, person_code, first_name, last_name, dob) VALUES (13, 'AP4353', 'Test', 'Raphael', '2021-05-28');
INSERT INTO public.patient (id, person_code, first_name, last_name, dob) VALUES (14, 'AP2020', 'final test', 'final after jar', '2021-05-29');


--
-- TOC entry 2994 (class 0 OID 41865)
-- Dependencies: 201
-- Data for Name: study; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.study (id, patient_id, name, description, created_date, modified_date) VALUES (15, 1, 'X-Ray', 'Bla Bla', '2021-05-25 01:37:11.071', '2021-05-25 01:37:11.072');
INSERT INTO public.study (id, patient_id, name, description, created_date, modified_date) VALUES (16, 1, 'CT Scan', 'test 2', '2021-05-25 22:32:11.167', '2021-05-25 22:32:11.167');
INSERT INTO public.study (id, patient_id, name, description, created_date, modified_date) VALUES (17, 2, 'Scan', 'Bla Bla', '2021-05-26 14:02:50.045', '2021-05-26 14:02:50.045');
INSERT INTO public.study (id, patient_id, name, description, created_date, modified_date) VALUES (18, 2, 'Sono', 'Bla Bla', '2021-05-26 14:24:46.661', '2021-05-26 14:24:46.663');
INSERT INTO public.study (id, patient_id, name, description, created_date, modified_date) VALUES (19, 2, 'Sono2', 'Bla Bla', '2021-05-26 14:25:23.063', '2021-05-26 14:25:23.063');
INSERT INTO public.study (id, patient_id, name, description, created_date, modified_date) VALUES (20, 2, 'Scan 1', 'Bla Bla', '2021-05-26 14:33:19.646', '2021-05-26 14:33:19.646');
INSERT INTO public.study (id, patient_id, name, description, created_date, modified_date) VALUES (21, 2, 'X-Ray', 'Bla Bla', '2021-05-26 14:42:26.105', '2021-05-26 14:42:26.105');
INSERT INTO public.study (id, patient_id, name, description, created_date, modified_date) VALUES (22, 2, 'X-Ray2343', 'Bla Bla', '2021-05-26 14:46:51.877', '2021-05-26 14:46:51.877');
INSERT INTO public.study (id, patient_id, name, description, created_date, modified_date) VALUES (23, 2, 'test', 'Bla Bla', '2021-05-26 14:54:44.202', '2021-05-26 14:54:44.202');
INSERT INTO public.study (id, patient_id, name, description, created_date, modified_date) VALUES (24, 2, 'ffghjg', 'Bla Bla', '2021-05-26 14:55:58.908', '2021-05-26 14:55:58.908');
INSERT INTO public.study (id, patient_id, name, description, created_date, modified_date) VALUES (25, 2, 'mukid', 'Bla Bla', '2021-05-26 15:13:58.713', '2021-05-26 15:13:58.713');
INSERT INTO public.study (id, patient_id, name, description, created_date, modified_date) VALUES (26, 2, 'X-Ray', 'Bla Bla', '2021-05-26 15:31:53.461', '2021-05-26 15:31:53.461');
INSERT INTO public.study (id, patient_id, name, description, created_date, modified_date) VALUES (27, 2, 'manaf', 'Bla Bla', '2021-05-26 15:43:46.952', '2021-05-26 15:43:46.952');
INSERT INTO public.study (id, patient_id, name, description, created_date, modified_date) VALUES (28, 4, 'CT Scan Mukid', 'dgrftht', '2021-05-27 03:57:19.576', '2021-05-27 03:57:19.576');
INSERT INTO public.study (id, patient_id, name, description, created_date, modified_date) VALUES (29, 2, 'Blood Grouping', 'Test Edit', '2021-05-27 03:58:11.595', '2021-05-27 03:58:11.595');
INSERT INTO public.study (id, patient_id, name, description, created_date, modified_date) VALUES (30, 6, 'CT Scan Hector', 'CT Scan Hector', '2021-05-27 04:01:45.309', '2021-05-27 04:01:45.309');
INSERT INTO public.study (id, patient_id, name, description, created_date, modified_date) VALUES (31, 6, 'CT Scan', 'CT Scan Hector', '2021-05-27 04:02:03.438', '2021-05-27 04:02:03.438');
INSERT INTO public.study (id, patient_id, name, description, created_date, modified_date) VALUES (33, 4, 'mukid-manaf', 'fgbdf', '2021-05-28 14:44:47.118', '2021-05-28 14:44:47.118');
INSERT INTO public.study (id, patient_id, name, description, created_date, modified_date) VALUES (32, 2, 'yyyyyy', 'zzzzzzzz', '2021-05-28 14:46:06.06', '2021-05-28 14:46:06.06');
INSERT INTO public.study (id, patient_id, name, description, created_date, modified_date) VALUES (34, 7, 'X-Ray', 'dfvgdsf', '2021-05-28 15:40:53.404', '2021-05-28 15:40:53.404');
INSERT INTO public.study (id, patient_id, name, description, created_date, modified_date) VALUES (35, 9, 'CT Scan', 'dfgdsfg', '2021-05-28 15:57:45.58', '2021-05-28 15:57:45.58');
INSERT INTO public.study (id, patient_id, name, description, created_date, modified_date) VALUES (36, 2, 'CT Scan mmm', 'fgbfsdg', '2021-05-28 22:27:08.874', '2021-05-28 22:27:08.874');
INSERT INTO public.study (id, patient_id, name, description, created_date, modified_date) VALUES (37, 14, 'On Jar', 'gtethjry', '2021-05-29 15:07:02.501', '2021-05-29 15:07:02.501');


--
-- TOC entry 3003 (class 0 OID 0)
-- Dependencies: 202
-- Name: patient_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.patient_seq', 14, true);


--
-- TOC entry 3004 (class 0 OID 0)
-- Dependencies: 203
-- Name: study_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.study_seq', 37, true);


--
-- TOC entry 2857 (class 2606 OID 41862)
-- Name: patient pk_patient_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.patient
    ADD CONSTRAINT pk_patient_id PRIMARY KEY (id);


--
-- TOC entry 2861 (class 2606 OID 41869)
-- Name: study pk_study_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.study
    ADD CONSTRAINT pk_study_id PRIMARY KEY (id);


--
-- TOC entry 2859 (class 2606 OID 41864)
-- Name: patient uq_patient_person_code; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.patient
    ADD CONSTRAINT uq_patient_person_code UNIQUE (person_code);


--
-- TOC entry 2862 (class 2606 OID 41870)
-- Name: study fk_study_person_id_with_patient_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.study
    ADD CONSTRAINT fk_study_person_id_with_patient_id FOREIGN KEY (patient_id) REFERENCES public.patient(id);


-- Completed on 2021-05-29 22:34:19

--
-- PostgreSQL database dump complete
--

