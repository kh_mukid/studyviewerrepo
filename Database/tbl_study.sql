-- Table: public.study

-- DROP TABLE public.study;

CREATE TABLE public.study
(
    id bigint NOT NULL,
    patient_id bigint NOT NULL,
    name character varying(50) COLLATE pg_catalog."default" NOT NULL,
    description character varying(200) COLLATE pg_catalog."default",
    created_date timestamp without time zone NOT NULL,
    modified_date timestamp without time zone NOT NULL,
    CONSTRAINT pk_study_id PRIMARY KEY (id),
    CONSTRAINT fk_study_person_id_with_patient_id FOREIGN KEY (patient_id)
        REFERENCES public.patient (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public.study
    OWNER to postgres;