-- SEQUENCE: public.study_seq

-- DROP SEQUENCE public.study_seq;

CREATE SEQUENCE public.study_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.study_seq
    OWNER TO postgres;