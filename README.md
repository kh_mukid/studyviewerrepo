# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* A patient diagnostic study viewer application.
* Version 1

## How do I get set up? ##

### Requirements ###

* [JDK 1.8](https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html)
* [Gradle](https://gradle.org/)
* [JUnit5](https://junit.org/junit5/)
* [PostGreSQL 13](https://www.postgresql.org/)
* [ReactJS 17.0.2](https://reactjs.org/)
* [Node 14.7](https://nodejs.org/en/)
* [NPM 6.14.13](https://www.npmjs.com/)

### Database Creation ###

### Create DB with Data ###

* Step 1: In PG Admin Tool create a database "**study_viewer**", DB creation script added under repository location (**studyviewerrepo/src/main/Database/**)
* Step 2: Open query tool under "**study_viewer**" DB and then run "**studyviewerrepo/src/main/Database/local_backup_full_with_insert.sql**"

## OR ##

### Create Vanila DB ###

* Step 1: In PG Admin Tool create a database "**study_viewer**" script added under repository location (**studyviewerrepo/src/main/Database/**)
* Step 2: Open query tool under "**study_viewer**" DB and then run below scripts from "**studyviewerrepo/src/main/Database/**"
* **patient.sql**
* **study.sql**
* **patient_seq.sql**
* **study_seq.sql**

### Project Run From Jar ###

* If above requirements filled, we can easyly run projects from jar. Commands below... JAR Location ()
* ** Notes:** In Jar PostgreSQL DB User: **postgres**, pass: **postgres**,
```
java -jar studyviewer.jar

Application will run under (http://localhost:8080)
```

### Project Run From IDE ###
*Step 1: Load project into IDE(Recommend: IntllijIDE)
*Step 2: Change the application.properties (**studyviewerrepo/studyviewer/src/main/resources/application.properties**) file for your own DB credential setup.
*Step 3: Now in your IDE terminal or any command terminal set location into (**studyviewerrepo/studyviewer**) and then run below command for build and standalone jar creation.
```
gradlew clean build
```
*if build success, It will first run npm tasks and then build both frontend and backend project into a standalone application.
*Above command also run the test also. As this POC test task I just write four test scenario.
*Step 4: Now if build success, you are ready to run project from IDE.

### Final Notes ###

* Feel free to communicate with me for any guidlines. Conatact info below: 
```
KH. AL MUKID
Skype: kh.almukid
Email: kh.mukid@gmail.com
Phone: +8801712896588 (Whatsapp)
```

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact