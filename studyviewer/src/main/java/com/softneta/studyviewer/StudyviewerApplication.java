package com.softneta.studyviewer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudyviewerApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudyviewerApplication.class, args);
	}

}
