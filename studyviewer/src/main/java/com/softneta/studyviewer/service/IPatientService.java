package com.softneta.studyviewer.service;

import com.softneta.studyviewer.model.Patient;

public interface IPatientService {

  boolean createPatient(Patient patient);

  Iterable<Patient> getAll();

  Long getPatientIdByPersonCode(String personCode);
}
