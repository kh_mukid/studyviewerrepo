package com.softneta.studyviewer.service.impl;

import com.softneta.studyviewer.model.Patient;
import com.softneta.studyviewer.repository.PatientRepository;
import com.softneta.studyviewer.service.IPatientService;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class PatientService implements IPatientService {

  private final PatientRepository repository;

  public PatientService(PatientRepository repository) {
    this.repository = repository;
  }

  @Override
  public boolean createPatient(Patient patient) {
    return repository.save(patient).getId() > 0;
  }

  @Override
  public Iterable<Patient> getAll() {
    return repository.findAll();
  }

  @Override
  public Long getPatientIdByPersonCode(String personCode) {
    Patient patient = repository.getPatientByPersonCode(personCode);

    return patient != null ? patient.getId() : null;
  }
}
