package com.softneta.studyviewer.service;

import com.softneta.studyviewer.model.Study;

public interface IStudyService {

  boolean createUpdateStudy(Study study);

  Iterable<Study> getAll();
}
