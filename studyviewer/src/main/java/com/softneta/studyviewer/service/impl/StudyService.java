package com.softneta.studyviewer.service.impl;

import com.softneta.studyviewer.model.Study;
import com.softneta.studyviewer.repository.StudyRepository;
import com.softneta.studyviewer.service.IStudyService;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class StudyService implements IStudyService {

  private final StudyRepository repository;

  public StudyService(StudyRepository repository) {
    this.repository = repository;
  }

  @Override
  public boolean createUpdateStudy(Study study) {
    Study retStudy = repository.save(study);
    return retStudy != null && retStudy.getId() > 0;
  }

  @Override
  public Iterable<Study> getAll() {
    return repository.findByOrderByCreatedDateDesc();
  }

}
