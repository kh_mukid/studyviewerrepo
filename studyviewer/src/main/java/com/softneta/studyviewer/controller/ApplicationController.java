package com.softneta.studyviewer.controller;

import com.softneta.studyviewer.model.Patient;
import com.softneta.studyviewer.model.Study;
import com.softneta.studyviewer.service.IPatientService;
import com.softneta.studyviewer.service.IStudyService;
import com.softneta.studyviewer.util.DateUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ApplicationController {

  private final IStudyService studyService;
  private final IPatientService patientService;

  public ApplicationController(IStudyService studyService, IPatientService patientService) {
    this.studyService = studyService;
    this.patientService = patientService;
  }

  @GetMapping("/study")
  public ResponseEntity<Iterable<Study>> getAllStudy() {
    return ResponseEntity.status(HttpStatus.OK).body(studyService.getAll());
  }

  @GetMapping("/patient")
  public ResponseEntity<Iterable<Patient>> getAllPatient() {
    return new ResponseEntity<>(patientService.getAll(), HttpStatus.OK);
  }

  @PostMapping("/createPatient")
  public ResponseEntity<Boolean> createPatient(@RequestBody Patient patient) {
    if (patient == null || patient.getPersonCode() == null) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<>(patientService.createPatient(patient), HttpStatus.OK);
  }

  @PostMapping("/createUpdateStudy")
  public ResponseEntity<Boolean> createUpdateStudy(@RequestBody Study study) {
    if (study == null || study.getPatientId() == null) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    study.setCreatedDate(DateUtils.getDateTimeOnly());
    study.setModifiedDate(DateUtils.getDateTimeOnly());
    return new ResponseEntity<>(studyService.createUpdateStudy(study), HttpStatus.OK);
  }

  @PostMapping("/getPatientIdByPersonCode")
  public ResponseEntity<Long> getPatientIdByPersonCode(@RequestBody Patient patient) {
    if (patient == null) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<>(patientService.getPatientIdByPersonCode(patient.getPersonCode()),
        HttpStatus.OK);
  }

}
