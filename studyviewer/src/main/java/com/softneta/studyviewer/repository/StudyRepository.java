package com.softneta.studyviewer.repository;

import com.softneta.studyviewer.model.Study;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudyRepository extends CrudRepository<Study, Long> {

  Iterable<Study> findByOrderByCreatedDateDesc();

  Study getById(long id);
}
