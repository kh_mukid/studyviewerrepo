package com.softneta.studyviewer.repository;

import com.softneta.studyviewer.model.Patient;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PatientRepository extends CrudRepository<Patient, Long> {

  Patient getPatientByPersonCode(String personCode);
}
