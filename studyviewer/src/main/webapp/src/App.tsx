import './App.css';
import './Assests/css/styles.css';
import './Assests/font/bootstrap-icons.css';
import { BrowserRouter, Route } from 'react-router-dom';
import Home from './components/Home';
import DefaultLayout from './components/DefaultLayout';
import Patient from './components/Patient';
import Study from './components/Study';
import { createBrowserHistory } from 'history';
import React from 'react';

const App = () => {

  const history = createBrowserHistory({ basename: "/" });
  return (
    <BrowserRouter>
      <DefaultLayout>
        <Route exact path='/' component={Home} />
        <Route path='/patient' component={Patient} />
        <Route path='/study' component={Study} />

      </DefaultLayout>
    </BrowserRouter>
  )
};

export default App;
