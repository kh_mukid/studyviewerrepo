export interface Patient {
  id?: string;
  personCode?: string;
  firstName?: string;
  lastName?: string;
  dateOfBirth?:string;
}

/*
* Name as DTO due to name conflicting with component
* */
export interface StudyDTO {
  id?: string;
  patientId?: string;
  studyName?: string;
  description?: string;
  createdDate?:string;
  modifiedDate?: string;
  patient?: Patient;
}
