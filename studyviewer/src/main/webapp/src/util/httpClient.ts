﻿const commonHeaders: any = {
    'Content-Type': 'application/json', 'pragma': 'no-cache', 'cache-control': 'no-cache'
}

export function get(url: string, requestBody?: Object, headers?: Object) {
    return sendRequest('GET', url, requestBody, headers);
}

export function post(url: string, requestBody?: Object, headers?: Object) {
    return sendRequest('POST', url, requestBody, headers);
}

export function put(url: string, requestBody?: Object, headers?: Object) {
    return sendRequest('PUT', url, requestBody, headers);
}

export function _delete(url: string, requestBody?: Object, headers?: Object) {
    return sendRequest('DELETE', url, requestBody, headers);
}

export function sendRequest(httpMethod: string, url: string, requestBody?: Object, headers?: Object) {
    const requestHeaders = Object.assign(commonHeaders, headers);
    const requestOptions = {
        method: httpMethod,
        headers: requestHeaders,
        body: JSON.stringify(requestBody)
    };
  return fetch(url, requestOptions).then(handleResponse);
}

function handleResponse(response: any) {
    return response.text().then((text: string) => {
        if (!response.ok) {
            try {
                const data = text && JSON.parse(text);
                const error = (data && data.message) || response.statusText;
                return Promise.reject(error);
            } catch (e) {

                let statusText = (response.statusText !== "") ? response.statusText : (response.status == 400) ? "Bad Resquest" : "Unexpected API Error"
                const error = {

                    statusCode: response.status,
                    errorMessage: statusText
                }
                return Promise.reject(error);
            }
        }
        const data = text && JSON.parse(text);
        return data;
    });
}