﻿import * as httpClient from '../util/httpClient';
import {sendRequest} from "../util/httpClient";
import {StudyDTO} from "../models/model";

export const getStudy = () => {
    return httpClient.sendRequest('GET', `api/study`);
}

export const createUpdateStudy = (requestBody: any) => {
    return sendRequest('post', 'api/createUpdateStudy', requestBody);
}
