﻿import * as httpClient from '../util/httpClient';


export const getPatient = () => {
  return httpClient.sendRequest('GET', `api/patient`);
}

export const getPatientIdByPersonCode = ( personCode?: string ) => {
  const requestBody = { personCode };
  return httpClient.sendRequest('POST', `api/getPatientIdByPersonCode`, requestBody);
}
