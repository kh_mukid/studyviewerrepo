import React, {useEffect, useState} from 'react';
import {data} from "../services/data";
import * as patientService from "../services/PatientService";
import {Button, Modal} from "react-bootstrap";
import StudyForm from "./StudyForm";
import PatientForm from "./PatientForm";

const Patient = (props: any) => {

  const [isLoading, setIsLoading] = useState(false);
  const [patientList, setPatientList] = useState([]);

  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const ensureDataFetched = () => {
    setIsLoading(true);

    patientService.getPatient()
    .then((data: any) => {
      setIsLoading(false);
      setPatientList(data);

    }).catch(function (error: any) {
      setTimeout(function () {
        setIsLoading(false);
      }, 2000);
    });
  }

  useEffect(() => {
    if (!show) {
      ensureDataFetched();
    }
  }, [show]);

  return (
      <div className="py-3">
        <h1>Patient</h1>

        {!isLoading ? <div className="table-responsive">
          <table className="table table-bordered table-hover">
            <tr>
              <th>Id</th>
              <th>Person Code</th>
              <th>Name</th>
              <th>Date Of Birth</th>
            </tr>
            {patientList && patientList.length > 0 && patientList.map((patient: any, index) => (
                <tr>
                  <td> {patient.id}</td>
                  <td> {patient.personCode}</td>
                  <td> {patient.firstName}{" "}{patient.lastName}</td>
                  <td> {patient.dateOfBirth ? patient.dateOfBirth.substring(0, 10) : null}</td>
                </tr>
            ))}
          </table>
        </div> : <div>Loading . . .</div>}
        <div className="col-md-12">
          <Button variant="primary" onClick={handleShow}>
            Create Patient
          </Button>

          <Modal show={show} onHide={handleClose}>
            <Modal.Header>
              <Modal.Title>Insert Patient Study</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <PatientForm handleClose={handleClose}/>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                Close
              </Button>
              {/*<Button variant="primary" onClick={handleClose}>
                Save Changes
              </Button>*/}
            </Modal.Footer>
          </Modal>
        </div>
      </div>
  );
}


export default Patient;
