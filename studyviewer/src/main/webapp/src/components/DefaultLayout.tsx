import React from 'react';
import { Container } from 'react-bootstrap';
import Footer from './Footer';
import Navber from './Navber';

const DefaultLayout = (props: { children?: React.ReactNode }) => {

    return (
        <React.Fragment>
            <Navber />
            <section className="pt-4">
                    
                        <Container className="container px-lg-5">
                        <div className="row gx-lg-5">
                            {props.children}
                            </div>
                        </Container>
                   
            </section>
            <Footer />
        </React.Fragment>
    )
};

export default DefaultLayout;
