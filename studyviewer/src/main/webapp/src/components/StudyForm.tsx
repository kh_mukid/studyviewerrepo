import React, {useState} from "react";
import {useForm} from 'react-hook-form';
import * as patientService from "../services/PatientService";
import * as studyService from "../services/StudyService";

const StudyForm = (props: any) => {

  const {register, handleSubmit, formState: {errors}} = useForm();
  const [patientId, setPatientId] = useState(props.editStudy?.patientId ? props.editStudy?.patientId : 0);
  const [isShow, setIsShow] = useState(false);
  const [studyId, setStudyId] = useState(props.editStudy?.id ? props.editStudy?.id : null);

  const onInputChange = (event: any) => {
    const {name, value} = event.target;
    getPatientIdByPersonCode(value);
  };

  const getPatientIdByPersonCode = (personCode: any) => {
    patientService.getPatientIdByPersonCode(personCode)
    .then((data: any) => {
      console.log(data);
      setPatientId(data == null || data == '' ? null : data);
    }).catch(function (error: any) {
      console.log(error);
    });
  };

  const onSubmit = (studyData: any) => {
    const requestBody = {
      "id": studyId,
      "patientId": patientId,
      "studyName": studyData["StudyName"],
      "description": studyData["description"]
    };

    studyService.createUpdateStudy(requestBody).then((data: boolean) => {
      if (data) {
        setIsShow(true);
        setTimeout(function () {
          props.handleClose();
        }, 2000);
      }
    }).catch(function (error) {
      console.log(error);
    });
  }
  return (
      <React.Fragment>
        <div className="row py-3">
          {isShow &&
          <div className="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Success!</strong> Study Creation done.
            <button type="button" className="close right" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true"><i className="bi bi-x"></i></span>
            </button>
          </div>
          }

          <div className="col-md-12">

            <form onSubmit={handleSubmit(onSubmit)}>
              <div className="form-group">
                <label htmlFor="person_code">Person Code</label>
                <input type="text" className="form-control" id="personCode"
                       defaultValue={props.editStudy?.patient?.personCode}
                       aria-describedby="PersonCode" placeholder="Enter person code"
                       {...register("PersonCode", {required: true})}
                       onChange={(e) => {
                         onInputChange(e);
                       }}/>
                {errors.PersonCode &&
                <span className="invalid-feedback">This field is required</span>}
                {patientId == null &&
                <span className="invalid-feedback">Invalid Person Code</span>}
              </div>
              <div className="form-group">
                <label htmlFor="study_name">Study Name</label>
                <input type="text" className="form-control" id="studyName"
                       defaultValue={props.editStudy?.studyName}
                       aria-describedby="StudyName" placeholder="Enter Study Name"
                       {...register("StudyName", {required: true})} />
                {errors.StudyName &&
                <span className="invalid-feedback">This field is required</span>}
              </div>

              <div className="form-group">
                <label htmlFor="description">Description</label>
                <textarea className="form-control" id="description"
                          defaultValue={props.editStudy?.description}
                          aria-describedby="description" placeholder="Enter description"
                          {...register("description")} />

              </div>
              <div className="form-group pt-2">
                <input className="btn btn-primary" type="submit"/>
              </div>
            </form>
          </div>
        </div>
      </React.Fragment>
  );
}
export default StudyForm;
