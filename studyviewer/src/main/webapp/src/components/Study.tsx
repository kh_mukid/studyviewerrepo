import React, {useEffect, useState} from 'react';
import * as StudyServices from '../services/StudyService';
import {Button, Modal} from "react-bootstrap";
import StudyForm from "./StudyForm";
import {StudyDTO} from "../models/model";

const Study = (props: any) => {

  const [isLoading, setisLoading] = useState(false);
  const [studyList, setStudyList] = useState([]);
  const [editStudy, setEditStudy] = useState({} as StudyDTO);


  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const handleEdit = (study: StudyDTO) => {
    setEditStudy(study);
  }

  const ensureDataFetched = () => {
    setisLoading(true);

    StudyServices.getStudy()
    .then((data: any) => {
      setisLoading(false);
      setStudyList(data);

    }).catch(function (error: any) {
      setTimeout(function () {
        setisLoading(false);
      }, 2000);
    });
  }

  useEffect(() => {
    if (!show) {
      ensureDataFetched();
    }
  }, [show]);

  return (
      <div className="row py-3">
        <h1>Study</h1>
        <p>This is patient study view</p>

        {!isLoading ? <div className="col-md-12 table-responsive">
          <table className="table table-bordered table-striped table-hover">
            <tr>
              <th>Person Code</th>
              <th>Patient Name</th>
              <th>Date Of Birth</th>
              <th>Study Name</th>
              <th>Study Creation</th>
              <th>Study Modification</th>
              <th></th>
            </tr>
            {studyList && studyList.map((study: StudyDTO, index) => (

                <tr>
                  <td>{study.patient?.personCode}</td>
                  <td>{study.patient?.firstName}{" "}{study.patient?.lastName}</td>
                  <td>{study.patient?.dateOfBirth ? study.patient?.dateOfBirth.substring(0, 10) : null}</td>
                  <td>{study.studyName}</td>
                  <td>{study.createdDate}</td>
                  <td>{study.modifiedDate}</td>
                  <td>
                    <button className="btn" onClick={() => {
                      handleShow();
                      handleEdit(study);
                    }}><i className="bi bi-pencil-square"></i></button>
                  </td>
                </tr>
            ))}
          </table>
        </div> : <div>Loading . . .</div>}
        <div className="col-md-12">
          <Button variant="primary" onClick={handleShow}>
            Create Study
          </Button>

          <Modal show={show} onHide={handleClose}>
            <Modal.Header>
              <Modal.Title>Insert Patient Study</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <StudyForm handleClose={handleClose} editStudy={editStudy}/>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                Close
              </Button>
            </Modal.Footer>
          </Modal>
        </div>

      </div>
  );
}

export default Study;
