import React, {useState} from "react";
import {useForm} from 'react-hook-form';
import {sendRequest} from "../util/httpClient";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import * as patientService from "../services/PatientService";

const PatientForm = (props: any) => {

  const {register, handleSubmit, formState: {errors}} = useForm();
  const [isShow, setIsShow] = useState(false);
  const [isDobError, setIsDobError] = useState(false);
  const [date, setDate] = useState(new Date());
  const [patientId, setPatientId] = useState( null);

  const onInputChange = (e: any) => {
    if (e != null) {
      setDate(e);
      setIsDobError(false);
    } else {
      setIsDobError(true);
    }
  };

  const onPersonCodeChange = (event: any) => {
    const {name, value} = event.target;
    getPatientIdByPersonCode(value);
  };
  const getPatientIdByPersonCode = (personCode: any) => {
    patientService.getPatientIdByPersonCode(personCode)
    .then((data: any) => {
      setPatientId(data == null || data == '' ? null : data);
    }).catch(function (error: any) {
      console.log(error);
    });
  };

  const onSubmit = (patientData: any) => {

    const requestBody = {
      "personCode": patientData["personCode"],
      "firstName": patientData["firstName"],
      "lastName": patientData["lastName"],
      "dateOfBirth": date
    }
    return sendRequest('post', 'api/createPatient', requestBody).then((data: boolean) => {

      if (data) {
        setIsShow(true);
        setTimeout(function () {
          props.handleClose();
        }, 2000);

      }

    }).catch(function (error) {
      console.log(error);
    });
  }
  return (
      <React.Fragment>
        <div className="row py-3">
          {isShow &&
          <div className="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Success!</strong>  Study Creation done.
            <button type="button" className="close right" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true"><i className="bi bi-x"></i></span>
            </button>
          </div>
          }
          <div className="col-md-12">
            <form onSubmit={handleSubmit(onSubmit)}>
              <div className="form-group">
                <label htmlFor="person_code">Person Code</label>
                <input type="text" className="form-control" id="personCode"

                       aria-describedby="PersonCode" placeholder="Enter person code"
                       {...register("personCode", {required: true})}
                       onChange={(e) => {
                         onPersonCodeChange(e);
                       }}/>
                {errors.personCode &&
                <span className="invalid-feedback">This field is required</span>}
                {patientId &&
                <span className="invalid-feedback">You already take this person code.</span>}
              </div>
              <div className="form-group">
                <label htmlFor="first_name">First Name</label>
                <input type="text" className="form-control" id="firstName"
                       aria-describedby="FirstName" placeholder="Enter First Name"
                       {...register("firstName", {required: true})} />
                {errors.firstName &&
                <span className="invalid-feedback">This field is required</span>}
              </div>

              <div className="form-group">
                <label htmlFor="last_name">Last Name</label>
                <input type="text" className="form-control" id="lastName"
                       aria-describedby="LastName" placeholder="Enter Last Name"
                       {...register("lastName", {required: true})} />
                {errors.lastName &&
                <span className="invalid-feedback">This field is required</span>}
              </div>


              <div className="form-group  pt-2">
                <label htmlFor="dob">Date Of Birth</label>
                <DatePicker name="dob" className="form-control" selected={date}
                            dateFormat="yyyy-MM-dd"
                            shouldCloseOnSelect={true} onChange={(e) => onInputChange(e)}/>

                {isDobError &&
                <span className="invalid-feedback">This field is required</span>}
              </div>
              <div className="form-group pt-2">
                <input className="btn btn-primary" type="submit"/>
              </div>
            </form>
          </div>
        </div>
      </React.Fragment>
  );
}
export default PatientForm;
