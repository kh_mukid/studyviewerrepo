package com.softneta.studyviewer.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import com.softneta.studyviewer.model.Patient;
import com.softneta.studyviewer.repository.PatientRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
class PatientServiceTest {

  @Autowired
  private PatientService patientService;

  @MockBean
  PatientRepository patientRepository;


  private Patient patient;

  private static final String VALID_PERSON_CODE = "AB123";
  private static final String IN_VALID_PERSON_CODE = "PPPPP";

  @BeforeEach
  void setUp() {
    patientService = new PatientService(patientRepository);

    patient = new Patient();

    patient.setId(1L);
    patient.setPersonCode("AB123");
    patient.setFirstName("Abul");
    patient.setLastName("Hussian");

  }


  @Test
  void get_patient_id_by_person_code_when_valid_person_code_given() {

    //when
    Mockito.when(patientRepository.getPatientByPersonCode(VALID_PERSON_CODE))
        .thenReturn(patient);
    long actualPatientId = patientService.getPatientIdByPersonCode(VALID_PERSON_CODE);

    //then
    assertNotNull(patient);
    assertEquals(patient.getId(), actualPatientId);
  }

  @Test
  void get_patient_id_by_person_code_when_in_valid_person_code_given() {

    //when
    Long actualPatientId = patientService.getPatientIdByPersonCode(IN_VALID_PERSON_CODE);

    //then
    assertNull(actualPatientId);
  }
}
