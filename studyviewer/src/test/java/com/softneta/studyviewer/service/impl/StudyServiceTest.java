package com.softneta.studyviewer.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.when;

import com.softneta.studyviewer.model.Study;
import com.softneta.studyviewer.repository.StudyRepository;
import com.softneta.studyviewer.util.DateUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class StudyServiceTest {

  @Mock
  StudyRepository studyRepository;

  private StudyService studyService;

  //Prepare data
  private Study study;


  @BeforeEach
  void setUp() {
    studyService = new StudyService(studyRepository);

    study = new Study();

    study.setPatientId(1L);
    study.setStudyName("From Service Test");
    study.setDescription("description");
    study.setCreatedDate(DateUtils.getDateTimeOnly());
    study.setModifiedDate(DateUtils.getDateTimeOnly());
  }

  @Test
  void testCreateUpdateStudy_when_create() {

    //when
    boolean expected = studyService.createUpdateStudy(study);

    //then
    assertFalse(expected);
  }

  @Test
  void testGetAll() {

    Iterable<Study> expectedStudies = studyRepository.findByOrderByCreatedDateDesc();
    //when
    when(studyRepository.findByOrderByCreatedDateDesc()).thenReturn(expectedStudies);
    Iterable<Study> actualStudy = studyService.getAll();

    //then
    assertEquals(expectedStudies, actualStudy);
  }

}
